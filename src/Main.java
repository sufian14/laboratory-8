import java.util.Arrays;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		// input an array
		Scanner input = new Scanner(System.in);
		System.out.println("Enter number of integers:");
		int a = input.nextInt();
		int[] array = new int[a];
		System.out.println("Enter integers:");
		for (int i = 0; i < a; i++) {
			array[i] = input.nextInt();
		}
		// sort array
		quickSort(array, 0, a - 1);
		//print result
		for (int i = 0; i < a; i++)
			System.out.println(array[i]);
	}

	//function sorts array by recursively sorting left part and right part
	public static void quickSort(int arr[], int begin, int end) {
		if (begin < end) {
			int partitionIndex = partition(arr, begin, end);
			quickSort(arr, begin, partitionIndex - 1);
			quickSort(arr, partitionIndex + 1, end);
		}
	}
	
	//function selects a pivot and moves smaller elements to the left 
	//and move bigger elements to the right of the pivot
	private static int partition(int arr[], int begin, int end) {
		int pivot = arr[end];
		int i = (begin - 1);

		for (int j = begin; j < end; j++) {
			if (arr[j] <= pivot) {
				i++;
				int swapTemp = arr[i];
				arr[i] = arr[j];
				arr[j] = swapTemp;
			}
		}

		int swapTemp = arr[i + 1];
		arr[i + 1] = arr[end];
		arr[end] = swapTemp;

		return i + 1;
	}
}
